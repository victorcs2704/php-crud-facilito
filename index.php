<?php
// Database
include('config/db.php');

// Set session
session_start();
if (isset($_POST['records-limit'])) {
    $_SESSION['records-limit'] = $_POST['records-limit'];
}

$limit = isset($_SESSION['records-limit']) ? $_SESSION['records-limit'] : 25;
$page = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;
$paginationStart = ($page - 1) * $limit;
$usuarios = $connection->query("SELECT * FROM dim_usuarios WHERE ESTADO = 1 ORDER BY ID DESC LIMIT $paginationStart, $limit")->fetchAll();
// Get total records
$sql = $connection->query("SELECT count(ID) AS id FROM dim_usuarios")->fetchAll();
$allRecrods = $sql[0]['id'];

// Calculate total pages
$totalPages = ceil($allRecrods / $limit);
// Prev + Next
$prev = $page - 1;
$next = $page + 1;
?>

<?php
include "./views/modals/addUser.php"
?>

<?php
include "./views/modals/editUser.php"
?>

<?php
include "./views/modals/deleteUser.php"
?>

<?php
include "./views/modals/viewUser.php"
?>

<?php
include "./views/template/header.php"
?>
<div class="container">
    <div class="jumbotron">
        <div class="card">
            <h2> PHP CRUD Bootstrap Usuario </h2>
        </div>

        <div class="card">
            <div class="card-body">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#userAddModal">
                    NUEVO USUARIO
                </button>
                <form action="index.php" method="post" class="float-right">
                    <select name="records-limit" id="records-limit" class="custom-select">
                        <option disabled selected>Límite de registros</option>
                        <?php foreach ([25, 50, 75, 100] as $limit) : ?>
                            <option <?php if (isset($_SESSION['records-limit']) && $_SESSION['records-limit'] == $limit) echo 'selected'; ?> value="<?= $limit; ?>">
                                <?= $limit; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </form>
            </div>
        </div>

        <div class="card">
            <div class="card-body">

                <table class="table table-hover table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col"> ID</th>
                            <th scope="col"> NOMBRES </th>
                            <th scope="col"> TELEFONO </th>
                            <th scope="col"> EMAIL </th>
                            <th scope="col"> VER </th>
                            <th scope="col"> EDITAR </th>
                            <th scope="col"> ELIMINAR </th>
                        </tr>
                    </thead>
                    <?php
                    if ($usuarios) {
                        foreach ($usuarios as $key => $row) {
                    ?>
                            <tbody>
                                <tr>
                                    <td> <?php echo $row['ID']; ?> </td>
                                    <td> <?php echo $row['NOMBRES']; ?> </td>
                                    <td> <?php echo $row['TELEFONO']; ?> </td>
                                    <td> <?php echo $row['CORREO']; ?> </td>
                                    <td>
                                        <button type="button" class="btn btn-info viewBtn" title="Ver Usuario"> <i class="bi bi-eye"></i>
                                        </button>
                                    <td>
                                        <button type="button" class="btn btn-success editBtn" title="Editar Usuario"> <i class="bi bi-pencil-square"></i> </button>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger deleteBtn" title="Eliminar Usuario"> <i class="bi bi-trash-fill"></i> </button>
                                    </td>
                                </tr>
                            </tbody>
                    <?php
                        }
                    } else {
                        echo "No Record Found";
                    }
                    ?>
                </table>
            </div>
        </div>

        <div class="container my-5">
            <!-- Pagination -->
            <nav aria-label="Page navigation example mt-5">
                <ul class="pagination justify-content-center">
                    <li class="page-item <?php if ($page <= 1) {
                                                echo 'disabled';
                                            } ?>">
                        <a class="page-link" href="<?php if ($page <= 1) {
                                                        echo '#';
                                                    } else {
                                                        echo "?page=" . $prev;
                                                    } ?>">Ant.</a>
                    </li>
                    <?php for ($i = 1; $i <= $totalPages; $i++) : ?>
                        <li class="page-item <?php if ($page == $i) {
                                                    echo 'active';
                                                } ?>">
                            <a class="page-link" href="index.php?page=<?= $i; ?>"> <?= $i; ?> </a>
                        </li>
                    <?php endfor; ?>
                    <li class="page-item <?php if ($page >= $totalPages) {
                                                echo 'disabled';
                                            } ?>">
                        <a class="page-link" href="<?php if ($page >= $totalPages) {
                                                        echo '#';
                                                    } else {
                                                        echo "?page=" . $next;
                                                    } ?>">Sig.</a>
                    </li>
                </ul>
            </nav>
        </div>

    </div>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>

<script>
    $(document).ready(function() {

        $('.viewBtn').on('click', function() {
            $('#userViewModal').modal('show');
            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function() {
                return $(this).text();
            }).get();

            $('#user_id').val(data[0]);
            $('#nombres').val(data[1]);
            $('#telefono').val(data[2]);
            $('#correo').val(data[3]);
        });

    });
</script>

<script>
    $(document).ready(function() {
        $('#records-limit').change(function() {
            $('form').submit();
        })
    });
</script>

<script>
    $(document).ready(function() {

        $('.deleteBtn').on('click', function() {

            $('#userDeleteModal').modal('show');

            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function() {
                return $(this).text();
            }).get();

            $('#delete_id').val(data[0]);
            $('#NOMBRE').text(data[1]);

        });
    });
</script>

<script>
    $(document).ready(function() {

        $('.editBtn').on('click', function() {

            $('#userEditModal').modal('show');

            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function() {
                return $(this).text();
            }).get();

            $('#update_id').val(data[0]);
            $('#NOMBRES').val(data[1]);
            $('#TELEFONO').val(data[2]);
            $('#CORREO').val(data[3]);
        });
    });
</script>

<?php
    include "./views/template/footer.php"
?>