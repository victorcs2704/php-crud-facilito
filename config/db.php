<?php
    $hostname = "localhost";
    $username = "root";
    $password = "secret";
    try {
        $connection = new PDO("mysql:host=$hostname;dbname=control_facilito_db", $username, $password);
        // set the PDO error mode to exception
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $connection->query("SET NAMES 'utf8'");
    } catch(PDOException $e) {
        echo "Database connection failed: " . $e->getMessage();
    }
?>