<?php
// Database
include('config/db.php');

// Set session
session_start();
if (isset($_POST['records-limit-ventas-anuales'])) {
    $_SESSION['records-limit-ventas-anuales'] = $_POST['records-limit-ventas-anuales'];
}

$limit = isset($_SESSION['records-limit-ventas-anuales']) ? $_SESSION['records-limit-ventas-anuales'] : 25;
$page = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;
$paginationStart = ($page - 1) * $limit;
$ventas = $connection->query("SELECT fvm.id_usuario AS ID,
fvm.ahno AS 'ANIO',
du.NOMBRES AS 'NOMBRE_USUARIO', 
SUM(fvm.cantidad_ventas) as 'TOTAL_VENTAS',
SUM(fvm.cantidad_productos) AS 'CANTIDAD_PRODUCTOS',
ROUND(SUM(fvm.monto), 2)  AS 'TOTAL_BS'
FROM
fact_venta_monthly fvm
INNER JOIN dim_usuarios du
WHERE du.ID = fvm.id_usuario
GROUP BY du.ID, fvm.ahno
ORDER BY du.ID ASC, fvm.ahno ASC LIMIT $paginationStart, $limit")->fetchAll();
// Get total records
$sql = $connection->query("SELECT count(distinct id_usuario) as 'ID' FROM fact_venta_monthly")->fetchAll();
$allRecrods = 73;

// Calculate total pages
$totalPages = ceil($allRecrods / $limit);
// Prev + Next
$prev = $page - 1;
$next = $page + 1;
?>


<?php
include "./views/template/header.php"
?>
<div class="container">
    <div class="jumbotron">
        <div class="card">
            <h2> Ventas por usuario por Año </h2>
        </div>
    </div>

    <div class="card">
        <div class="card-body">

            <form action="reportePorAnio.php" method="post" class="float-right">
                <select name="records-limit-ventas-anuales" id="records-limit-ventas-anuales" class="custom-select">
                    <option disabled selected>Límite de registros</option>
                    <?php foreach ([25, 50, 75, 100] as $limit) : ?>
                        <option <?php if (isset($_SESSION['records-limit-ventas-anuales']) && $_SESSION['records-limit-ventas-anuales'] == $limit) echo 'selected'; ?> value="<?= $limit; ?>">
                            <?= $limit; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </form>

        </div>
    </div>

    <div class="card">
        <div class="card-body">

            <table class="table table-hover table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col"> ID</th>
                        <th scope="col"> AÑO</th>
                        <th scope="col"> USUARIO </th>
                        <th scope="col"> TOTAL VENTAS </th>
                        <th scope="col"> CANT. PRODUCTOS </th>
                        <th scope="col"> TOTAL Bs. </th>
                    </tr>
                </thead>
                <?php
                if ($ventas) {
                    foreach ($ventas as $key => $row) {
                ?>
                        <tbody>
                            <tr>
                                <td> <?php echo $row['ID']; ?> </td>
                                <td> <?php echo $row['ANIO']; ?> </td>
                                <td> <?php echo $row['NOMBRE_USUARIO']; ?> </td>
                                <td> <?php echo $row['TOTAL_VENTAS']; ?> </td>
                                <td> <?php echo $row['CANTIDAD_PRODUCTOS']; ?> </td>
                                <td> <?php echo $row['TOTAL_BS']; ?> </td>
                            </tr>
                        </tbody>
                <?php
                    }
                } else {
                    echo "No Record Found";
                }
                ?>
            </table>
        </div>
    </div>

    <div class="container my-5">
        <!-- Pagination -->
        <nav aria-label="Page navigation example mt-5">
            <ul class="pagination justify-content-center">
                <li class="page-item <?php if ($page <= 1) {
                                            echo 'disabled';
                                        } ?>">
                    <a class="page-link" href="<?php if ($page <= 1) {
                                                    echo '#';
                                                } else {
                                                    echo "?page=" . $prev;
                                                } ?>">Ant.</a>
                </li>
                <?php for ($i = 1; $i <= $totalPages; $i++) : ?>
                    <li class="page-item <?php if ($page == $i) {
                                                echo 'active';
                                            } ?>">
                        <a class="page-link" href="reportePorAnio.php?page=<?= $i; ?>"> <?= $i; ?> </a>
                    </li>
                <?php endfor; ?>
                <li class="page-item <?php if ($page >= $totalPages) {
                                            echo 'disabled';
                                        } ?>">
                    <a class="page-link" href="<?php if ($page >= $totalPages) {
                                                    echo '#';
                                                } else {
                                                    echo "?page=" . $next;
                                                } ?>">Sig.</a>
                </li>
            </ul>
        </nav>
    </div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>

<script>
    $(document).ready(function() {
        $('#records-limit-ventas-anuales').change(function() {
            $('form').submit();
        })
    });
</script>
<?php
include "./views/template/footer.php"
?>