    <!-- VIEW POP UP FORM (Bootstrap MODAL) -->
    <div class="modal fade" id="userViewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> Ver informacion de Usuario </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                    <div class="modal-body">

                        <input type="hidden" name="user_id" id="user_id">

                        <div class="form-group">
                            <label> Nombres </label>
                            <input type="text" id="nombres" name="nombres" class="form-control" disabled>
                        </div>

                        <div class="form-group">
                            <label> Telefono </label>
                            <input type="text" id="telefono" name="telefono" class="form-control" disabled>
                        </div>

                        <div class="form-group">
                            <label> Email </label>
                            <input type="email" id="correo" name="correo" class="form-control" disabled>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>

            </div>
        </div>
    </div>