    <!-- Modal -->
    <div class="modal fade" id="userAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar información de usuario </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="createUser.php" method="POST">

                    <div class="modal-body">

                        <div class="form-group">
                            <label> Nombres </label>
                            <input type="text" name="nombres" class="form-control" placeholder="Introduce nombre completo" required>
                        </div>

                        <div class="form-group">
                            <label> Telefono </label>
                            <input type="number" name="telefono" class="form-control" placeholder="Introduce un numero telefonico" required onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" min="11111111" max="9999999999">
                        </div>

                        <div class="form-group">
                            <label> Email </label>
                            <input type="email" name="correo" class="form-control" placeholder="Introduce tu correo" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" name="insertData" class="btn btn-primary">Guardar Usuario</button>
                    </div>
                </form>

            </div>
        </div>
    </div>