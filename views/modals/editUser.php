    <!-- EDIT POP UP FORM (Bootstrap MODAL) -->
    <div class="modal fade" id="userEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> Editar informacion de usuario </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="updateUser.php" method="POST">

                    <div class="modal-body">

                        <input type="hidden" name="update_id" id="update_id">

                        <div class="form-group">
                            <label> Nombres </label>
                            <input type="text" id="NOMBRES" name="nombres" class="form-control" placeholder="Introduce nombre completo" required>
                        </div>

                        <div class="form-group">
                            <label> Telefono </label>
                            <input type="text" id="TELEFONO" name="telefono" class="form-control" placeholder="Introduce un numero telefonico" required onkeypress="return (event.charCode >= 48 && event.charCode <= 57)">
                        </div>

                        <div class="form-group">
                            <label> Email </label>
                            <input type="email" id="CORREO" name="correo" class="form-control" placeholder="Introduce tu correo" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" name="updateData" class="btn btn-primary">Actualizar usuario</button>
                    </div>
                </form>

            </div>
        </div>
    </div>